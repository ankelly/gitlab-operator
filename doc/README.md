# Operator Documentation

- [Known limitations](known-limitations.md): Lists known limitations of the Operator
- [Versioning and Release Info](developer/releases.md): Records notes concerning versioning and releasing the operator
- [Design decisions](design-decisions.md): Records design decisions regarding the structure, functionality, and feature implementation of this Operator
- [Security context constraints](security-context-constraints.md): Explains how SCCs are used within the GitLab Operator and in supporting Operators
- [Openshift cluster setup](openshift-cluster-setup.md): Provides instructions to launch an Openshift cluster
- [Installation](installation.md): Provides instructions for installing the GitLab Operator and CustomResource
- [Upgrades](upgrades.md): Demonstrates how to upgrade the GitLab Operator
- [Developer guide](developer-guide.md): Outlines the project structure and how to contribute
